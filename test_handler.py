import unittest
import json
from handler import hello


class TestHandler(unittest.TestCase):
    def test_hello(self):
        mock_body = body = {
            "message": "Go Serverless v1.0! Your function executed successfully!",
            "input": {}
        }
        response = hello({})
        self.assertEqual(response, {
            "statusCode": 200,
            "body": json.dumps(mock_body)
        })
